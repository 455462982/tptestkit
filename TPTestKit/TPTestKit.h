//
//  TPTestKit.h
//  TPTestKit
//
//  Created by tengpan on 2017/11/30.
//  Copyright © 2017年 tengpan. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for TPTestKit.
FOUNDATION_EXPORT double TPTestKitVersionNumber;

//! Project version string for TPTestKit.
FOUNDATION_EXPORT const unsigned char TPTestKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <TPTestKit/PublicHeader.h>

#import <TPTestKit/TPTestManager.h>
